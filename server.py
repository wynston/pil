from wsgiref.simple_server import make_server
from tg import expose, TGController, AppConfig
from watermark import add_watermark

class RootController(TGController):
    @expose()
    def index(self):
    	png_str = add_watermark("test2.png", "hello", out_file='watermark.jpg', angle=23, opacity=1)
        return "<h1>Hello World</h1><img src=\"data:image/jpeg;base64," + png_str + "\"></img>"

config = AppConfig(minimal=True, root_controller=RootController())

print "Serving on port 8080..."
httpd = make_server('', 8080, config.make_wsgi_app())
httpd.serve_forever()
