from PIL import Image, ImageDraw, ImageFont, ImageEnhance
import os, sys
import base64
import cStringIO
 
FONT = os.path.join(os.getcwd(), "example/lib/watermark", "heiti_GB18030.ttf")
 
def add_watermark(in_file, text, out_file='watermark.jpg', angle=23, opacity=1):
    angle = float(angle)
    opacity = float(opacity)
    img = Image.open(in_file).convert('RGB')
    watermark = Image.new('RGBA', img.size, (0,0,0,0))
    size = 2
    n_font = ImageFont.truetype(FONT, size)
    n_width, n_height = n_font.getsize(text)
    while n_width+n_height < watermark.size[0]:
        size += 2
        n_font = ImageFont.truetype(FONT, size)
        n_width, n_height = n_font.getsize(text)
    draw = ImageDraw.Draw(watermark, 'RGBA')
    draw.text(((watermark.size[0] - n_width) / 2,
              (watermark.size[1] - n_height) / 2),
              unicode(text,'UTF-8'), font=n_font, fill=(0,0,0,256))
    watermark = watermark.rotate(angle,Image.BICUBIC)
    alpha = watermark.split()[3]
    alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
    watermark.putalpha(alpha)
    buffer = cStringIO.StringIO()
    # Image.composite(watermark, img, watermark).save(out_file, 'JPEG')
    Image.composite(watermark, img, watermark).save(buffer, format="PNG")
    img_str = base64.b64encode(buffer.getvalue())
    return img_str



 
if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.exit('Usage: %s <input-image> <text> <output-image> ' \
                 '<angle> <opacity> ' % os.path.basename(sys.argv[0]))
    add_watermark(*sys.argv[1:])